package by.itstep.channel.app.repository;

import by.itstep.channel.app.entity.ChannelEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChannelRepository extends JpaRepository<ChannelEntity, Integer> {

    ChannelEntity findOneById(Integer id);

}



