package by.itstep.channel.app.repository;

import by.itstep.channel.app.entity.PostEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepository extends JpaRepository<PostEntity,Integer> {

    @Query(value = "Select * from post where id = :myId", nativeQuery = true)
    PostEntity findOneById(@Param("myId") Integer id);
}
