package by.itstep.channel.app.service.impl;

import by.itstep.channel.app.dto.post.PostCreateDto;
import by.itstep.channel.app.dto.post.PostFullDto;
import by.itstep.channel.app.entity.ChannelEntity;
import by.itstep.channel.app.entity.PostEntity;
import by.itstep.channel.app.mapper.PostMapper;
import by.itstep.channel.app.repository.ChannelRepository;
import by.itstep.channel.app.repository.PostRepository;
import by.itstep.channel.app.service.PostService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class PostServiceImpl implements PostService {

    private final PostRepository repository;
    private final ChannelRepository channelRepository;
    private final PostMapper mapper;

    @Override
    @Transactional
    public PostFullDto create(PostCreateDto createDto) {
        PostEntity postEntityToSave = mapper.toEntity(createDto);

        ChannelEntity channelEntity = channelRepository.findOneById(createDto.getChannelId());
        postEntityToSave.setChannel(channelEntity);

        PostEntity postEntitySaved = repository.save(postEntityToSave);
        return mapper.toDto(postEntitySaved);
    }
}
