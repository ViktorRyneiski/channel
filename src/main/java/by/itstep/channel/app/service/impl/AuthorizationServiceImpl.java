package by.itstep.channel.app.service.impl;

import by.itstep.channel.app.security.dto.AuthorizationRequest;
import by.itstep.channel.app.dto.user.Passport;
import by.itstep.channel.app.entity.UserEntity;
import by.itstep.channel.app.mapper.UserMapper;
import by.itstep.channel.app.repository.UserRepository;
import by.itstep.channel.app.service.AuthorizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.util.DigestUtils;

public class AuthorizationServiceImpl implements AuthorizationService {

    @Autowired
    private UserMapper mapper;

    @Autowired
    private UserRepository repository;

    @Override
    public Passport login(AuthorizationRequest request) {
        UserEntity foundEntity = repository.findByLogin(request.getLogin())
                .orElseThrow(() -> new BadCredentialsException("Wrong email or password"));

        String hashedPassword = DigestUtils.md5DigestAsHex(request.getPassword().getBytes());

        if (!hashedPassword.equals(foundEntity.getPassword())){
            throw new BadCredentialsException("Wrong email or password");
        }

        Passport passport = new Passport();
        passport.setLogin(foundEntity.getLogin());
        passport.setRole("Admin");

        String totalData = passport.getLogin() + passport.getRole();
        String hashSum = DigestUtils.md5DigestAsHex(totalData.getBytes());
        passport.setHashSum(hashSum);
        return passport;
    }

    @Override
    public boolean checkPassword(Passport passport) {
        String totalData = passport.getLogin() + passport.getRole();
        String hashSum = DigestUtils.md5DigestAsHex(totalData.getBytes());

        return hashSum.equals(passport.getHashSum());
    }
}
