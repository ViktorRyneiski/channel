package by.itstep.channel.app.service;

import by.itstep.channel.app.dto.post.PostCreateDto;
import by.itstep.channel.app.dto.post.PostFullDto;

public interface PostService {

    PostFullDto create(PostCreateDto createDto);

}
