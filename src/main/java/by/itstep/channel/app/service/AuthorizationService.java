package by.itstep.channel.app.service;

import by.itstep.channel.app.security.dto.AuthorizationRequest;
import by.itstep.channel.app.dto.user.Passport;
import org.springframework.stereotype.Service;

@Service
public interface AuthorizationService {

    Passport login(AuthorizationRequest request);

    boolean checkPassword(Passport passport);
}
