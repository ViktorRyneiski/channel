package by.itstep.channel.app.service;

import by.itstep.channel.app.dto.user.UserCreateDto;
import by.itstep.channel.app.dto.user.UserFullDto;
import by.itstep.channel.app.dto.user.UserUpdateDto;
import org.springframework.data.domain.Page;

import java.util.List;

public interface UserService {

    UserFullDto findById(Integer id);

    Page<UserFullDto> findAll(int page, int size);

    UserFullDto create(UserCreateDto createDto);

    UserFullDto update(UserUpdateDto updateDto);

    void deleteById(Integer id);

    UserFullDto findUserByLogin(String name);
}
