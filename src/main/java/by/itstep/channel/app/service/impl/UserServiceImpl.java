package by.itstep.channel.app.service.impl;

import by.itstep.channel.app.dto.user.UserCreateDto;
import by.itstep.channel.app.dto.user.UserFullDto;
import by.itstep.channel.app.dto.user.UserUpdateDto;
import by.itstep.channel.app.entity.UserEntity;
import by.itstep.channel.app.exception.EntityIsNotFoundException;
import by.itstep.channel.app.exception.UserCredentialsAreTakenException;
import by.itstep.channel.app.mapper.UserMapper;
import by.itstep.channel.app.repository.UserRepository;
import by.itstep.channel.app.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final UserMapper mapper;
    private UserEntity entityToSave;

    @Override
    @Transactional(readOnly = true)
    public UserFullDto findById(Integer id) {
        UserEntity foundEntity = userRepository.findById(id)
                .orElseThrow(() -> new EntityIsNotFoundException(String.format("User not found by id: %s",id)));
        log.info("UserServiceImpl -> found user: {}", foundEntity);
        return mapper.mapToFullDto(foundEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserFullDto> findAll(int page, int size) {
        Page<UserEntity> founEntities = userRepository.findAll(PageRequest.of(page,size));

        return founEntities.map(mapper::mapToFullDto);
    }

    @Override
    @Transactional
    public UserFullDto create(UserCreateDto createDto) {
        UserEntity entityToSave = mapper.mapToEntity(createDto);

        checkIfLoginOrEmailIsTaken(entityToSave);// TODO написать все сценарии с ожиданием RuntimeException (логин занят или емаил)

        String encodedPassword = DigestUtils.md5DigestAsHex(createDto.getPassword().getBytes());
        entityToSave.setPassword(encodedPassword);

        UserEntity savedEntity = userRepository.save(entityToSave);
        log.info("UserServiceImpl -> user {} successfully saved", savedEntity);
        return mapper.mapToFullDto(savedEntity);
    }

    @Override
    @Transactional
    public UserFullDto update(UserUpdateDto updateDto) {
        UserEntity entityToUpdate = userRepository.findOneById(updateDto.getId());
        if (entityToUpdate == null){
            throw new EntityIsNotFoundException("User not found by id: " +updateDto.getId());
        }

        entityToUpdate.setEmail(updateDto.getEmail());
        entityToUpdate.setImageUrl(updateDto.getImageUrl());

        checkIfLoginOrEmailIsTaken(entityToUpdate);

        UserEntity updatedEntity = userRepository.save(entityToUpdate);
        log.info("UserServiceImpl -> user {} successfully updated", updatedEntity);
        return mapper.mapToFullDto(updatedEntity);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        if (!userRepository.existsById(id)) {
            throw new EntityIsNotFoundException("Entity by id: " + id + " not found");
        } else {
            userRepository.deleteById(id);
        }

    }

    @Override
    public UserFullDto findUserByLogin(String name) {
        UserEntity foundEntity = userRepository.findByLogin(name)
                .orElseThrow(() -> new EntityIsNotFoundException(String.format("User not found by name: %s",name)));
        log.info("UserServiceImpl -> found user: {}", foundEntity);
        return mapper.mapToFullDto(foundEntity);
    }

    private void checkIfLoginOrEmailIsTaken(UserEntity entity) {
        //if (есть юзер с моим логином и емайлом и у него не мой id)
        List<UserEntity> foundUsers = userRepository.findByLoginOrEmail
                (entity.getLogin(),entity.getEmail());
//        for (UserEntity userEntity:foundUsers) {
//            if (!userEntity.getId().equals(entity.getId())){
//                throw new UserCredentialsAreTakenException("Email: " + entity.getEmail() + " | Login: " +entity.getLogin());
//            }
//        }

        boolean taken = foundUsers.stream()//TODO
                .anyMatch(user -> !user.getId().equals(entity.getId()));

        if (taken){
            throw new UserCredentialsAreTakenException("Email: " + entity.getEmail() + " | Login: " +entity.getLogin());
        }
    }
}
