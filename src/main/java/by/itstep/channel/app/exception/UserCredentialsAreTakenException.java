package by.itstep.channel.app.exception;

public class UserCredentialsAreTakenException extends RuntimeException{

    public UserCredentialsAreTakenException(String message) {
        super(message);
    }
}
