package by.itstep.channel.app.exception;

public class EntityIsNotFoundException extends RuntimeException{

    public EntityIsNotFoundException(String message) {
        super(message);
    }
}
