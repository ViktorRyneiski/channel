package by.itstep.channel.app.controller;

import by.itstep.channel.app.dto.user.UserCreateDto;
import by.itstep.channel.app.dto.user.UserFullDto;
import by.itstep.channel.app.dto.user.UserLoginDto;
import by.itstep.channel.app.dto.user.UserUpdateDto;
import by.itstep.channel.app.entity.UserEntity;
import by.itstep.channel.app.service.UserService;
import com.github.javafaker.Faker;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

//TODO rest template
@Api(description = "Controller dedicated to manage users")
@RestController
public class UserController {

    private Faker faker = new Faker();

    private UserService userService;

    @ApiOperation(value = "Find one user by id", notes = "Existing id must be specified")
    @GetMapping("/users/{id}")
    public UserFullDto getById(@PathVariable Integer id){
        return userService.findById(id);
    }

//    @GetMapping("/login")
//    public UserFullDto login(@RequestBody UserLoginDto loginDto){
//        return userService.findUserByLogin(loginDto);
//    }

    @GetMapping("/users")
    public Page<UserFullDto> findAll(@RequestParam int page, @RequestParam int size){// ...../users?page=7&size=10
        return userService.findAll(page,size);
    }

//    @ApiOperation(value = "Find all users by parameters", notes = "Actually it is gonna return all existings users")
//    @GetMapping("/users/filter")
//    public List<UserFullDto> findAllByName(@ApiParam(required = false,defaultValue = "Bob") @RequestParam String name,
//                                           @ApiParam(required = false) @RequestParam String email) {//опциональные параметры в отличии от @PathVariable
//        return userService.findAll();//плохо
//    }

    @GetMapping("/test-create")
    public void test(){
        for (int i = 0; i < 10; i++) {
            UserCreateDto createDto = new UserCreateDto();
            createDto.setEmail(faker.address().country());
            createDto.setLogin(faker.name().lastName());
            createDto.setPassword(faker.phoneNumber().phoneNumber());
            userService.create(createDto);
        }
    }

    @PostMapping("/users")
    public UserFullDto create(@Valid @RequestBody UserCreateDto createDto){
        return userService.create(createDto);
    }

    @PutMapping("/users")
    public UserFullDto update(@RequestBody UserUpdateDto updateDto){
        return userService.update(updateDto);
    }

    @DeleteMapping("/users/{id}")
    public void delete(@PathVariable Integer id){
        userService.deleteById(id);
    }
}
