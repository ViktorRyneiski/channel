package by.itstep.channel.app.security.dto;

import lombok.Data;

@Data
public class AuthenticationResponse {

    private String login;
    private String accessToken;
//    private String refreshToken;
}
