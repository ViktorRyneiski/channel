package by.itstep.channel.app.security.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class AuthorizationRequest {

    @NotEmpty
    private String login;

    @NotEmpty
    private String password;
}
