package by.itstep.channel.app.security;

import by.itstep.channel.app.entity.UserEntity;
import by.itstep.channel.app.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UserEntity foundEntity = userRepository.findByLogin(s)
                .orElseThrow(() -> new UsernameNotFoundException("Not found by username: " + s));

        return new JwtUserDetails(foundEntity);
    }
}
