package by.itstep.channel.app.security;

import by.itstep.channel.app.security.dto.AuthenticationResponse;
import by.itstep.channel.app.security.dto.AuthorizationRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class AuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @PostMapping("/login")
    public AuthenticationResponse login(@Valid @RequestBody AuthorizationRequest request){
        UsernamePasswordAuthenticationToken auth =
                new UsernamePasswordAuthenticationToken(request.getLogin(),request.getPassword());
        authenticationManager.authenticate(auth);

        //create token and return



        return null;
    }
}
