package by.itstep.channel.app.entity;

public enum ChannelEntityType {
    EDUCATION,
    PROGRAMMING,
}
