package by.itstep.channel.app.entity;

import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "channel")
public class ChannelEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Column(name = "type")
    @Enumerated(value = EnumType.STRING)
    private ChannelEntityType type;
    // создаем для личных целей, аккаунтов,
    @OneToMany(mappedBy = "channel", fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST}, orphanRemoval = true)
    private List<PostEntity> posts = new ArrayList<>();
}
