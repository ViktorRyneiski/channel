package by.itstep.channel.app.mapper;

import by.itstep.channel.app.dto.channel.ChannelCreateDto;
import by.itstep.channel.app.dto.channel.ChannelFullDto;
import by.itstep.channel.app.dto.channel.ChannelPreviewDto;
import by.itstep.channel.app.entity.ChannelEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(
        componentModel = "spring",
        uses = PostMapper.class
)
public interface ChannelMapper {

    @Mapping(target = "posts",ignore = true)
    @Mapping(target = "id",ignore = true)
    ChannelEntity toEntity(ChannelCreateDto createDto);

    ChannelFullDto toFullDto(ChannelEntity entity);

    ChannelPreviewDto toPreviewDto(ChannelEntity entity);


}
