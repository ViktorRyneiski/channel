package by.itstep.channel.app.mapper;

import by.itstep.channel.app.dto.post.PostCreateDto;
import by.itstep.channel.app.dto.post.PostFullDto;
import by.itstep.channel.app.entity.PostEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface PostMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "channel",ignore = true)
    PostEntity toEntity(PostCreateDto createDto);

    @Mapping(target = "channelId", source = "channel.id")
    PostFullDto toDto(PostEntity entity);
}
