package by.itstep.channel.app.mapper;

import by.itstep.channel.app.dto.user.UserCreateDto;
import by.itstep.channel.app.dto.user.UserFullDto;
import by.itstep.channel.app.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface UserMapper {

    @Mapping(target = "profileImageUrl",source = "imageUrl")
//    @Mapping(target = "phoneNumber",expression = "java(String.valueOf(userEntity.getId()*999))") // Пример как сделать из last & firstName -> name
    @Mapping(target = "phoneNumber",source = "id")
    UserFullDto mapToFullDto(UserEntity userEntity);

    @Mapping(target = "id",ignore = true)
    UserEntity mapToEntity(UserCreateDto createDto);

//    @Mapping(target = "role", source = "")
//    @Mapping(target = "hashSum", source = "")
//    Passport mapToPassport(AuthorizationRequest request);

    default String map(Integer number){
        return String.valueOf(number*999);
    }

}
