package by.itstep.channel.app.dto.channel;

import by.itstep.channel.app.entity.ChannelEntityType;
import lombok.Data;

@Data
public class ChannelPreviewDto {

    private Integer id;
    private String name;
    private ChannelEntityType type;
}
