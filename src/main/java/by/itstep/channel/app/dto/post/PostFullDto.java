package by.itstep.channel.app.dto.post;

import lombok.Data;

@Data
public class PostFullDto {

    private Integer id;
    private String title;
    private String content;
    private Integer channelId;
}
