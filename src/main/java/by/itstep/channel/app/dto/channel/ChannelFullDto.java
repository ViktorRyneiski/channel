package by.itstep.channel.app.dto.channel;

import by.itstep.channel.app.dto.post.PostFullDto;
import by.itstep.channel.app.entity.ChannelEntityType;
import by.itstep.channel.app.entity.PostEntity;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ChannelFullDto {

    private Integer id;
    private String name;
    private ChannelEntityType type;
    private List<PostFullDto> posts = new ArrayList<>();

}
