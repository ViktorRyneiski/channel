package by.itstep.channel.app.dto.user;

import lombok.Data;

import javax.persistence.Column;

@Data
public class ChangePasswordDto {

    private Integer id;
    private String oldPassword;
    private String newPassword;
}
