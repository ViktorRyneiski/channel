package by.itstep.channel.app.dto.user;

import lombok.Data;

import javax.persistence.Column;

@Data
public class UserFullDto {

    private Integer id;
    private String login;
    private String profileImageUrl;
    private String email;
    private String phoneNumber;
    private String password;
}
