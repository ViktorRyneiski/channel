package by.itstep.channel.app.dto.post;

import lombok.Data;

@Data
public class PostCreateDto {

    private String title;
    private String content;
    private Integer channelId;
}
