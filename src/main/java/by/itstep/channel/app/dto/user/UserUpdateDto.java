package by.itstep.channel.app.dto.user;

import lombok.Data;

import javax.persistence.Column;

@Data
public class UserUpdateDto {

    private Integer id;
    private String email;
    private String imageUrl;
}
