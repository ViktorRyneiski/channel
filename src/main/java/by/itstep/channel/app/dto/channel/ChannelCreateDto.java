package by.itstep.channel.app.dto.channel;

import by.itstep.channel.app.entity.ChannelEntityType;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
public class ChannelCreateDto {

    private String name;
    private ChannelEntityType type;
}
