package by.itstep.channel.app.dto.user;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class UserCreateDto {

    @ApiModelProperty(name = "Login",example = "bob_bobson",notes = "Must be unique")
    @NotEmpty(message = "Login can not be empty")
    private String login;

    @ApiModelProperty(example = "12345678")
    @NotEmpty(message = "password can not be empty")
    private String password;

    @ApiModelProperty(example = "http://image.jpg",notes = "Link to the image file")
    private String imageUrl;

    @ApiModelProperty(example = "bob@gmail.com",notes = "Email must be unique")
    @NotEmpty(message = "email can not be empty")
    private String email;
}
