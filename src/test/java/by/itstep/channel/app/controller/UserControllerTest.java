package by.itstep.channel.app.controller;

import by.itstep.channel.app.dto.user.UserCreateDto;
import by.itstep.channel.app.dto.user.UserFullDto;
import by.itstep.channel.app.entity.UserEntity;
import by.itstep.channel.app.repository.UserRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {

    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private UserRepository repository;

    @BeforeEach
    void clearDB() {
        repository.deleteAll();
    }

//    @Test
//    void testFindById_happyPath() throws Exception {
//        //given
//        UserEntity userToSave = new UserEntity();
//        userToSave.setEmail("bob@gmail.com");
//        userToSave.setLogin("bob@gmail.com");
//        userToSave.setPassword("bob@gmail.com");
//        userToSave.setImageUrl("bob@gmail.com");
//
//        UserEntity savedUser = repository.save(userToSave);
//
//        //when
//        MvcResult mvcResult = mockMvc.perform(get("/users/{id}", savedUser.getId()))
//                .andExpect(status().isOk())
//                .andReturn();
//
//        byte[] bytes = mvcResult.getResponse().getContentAsByteArray();
//        UserFullDto userFullDto = objectMapper.readValue(bytes, UserFullDto.class);
//
//        //then
//        Assertions.assertNotNull(userFullDto);
//        Assertions.assertNotNull(userFullDto.getId());
//        Assertions.assertEquals(savedUser.getId(), userFullDto.getId());
//    }

//    @Test
//    void testFindById_whenNotFound() throws Exception {
//        //given
//        int notExistingId = 1000;
//
//        //when
//        mockMvc.perform(get("/users/{id}", notExistingId))
//                .andExpect(status().isNotFound());
//        //then
//    }
//
//    @Test
//    void testCreate_happyPath() throws Exception {
//        //given
//        UserCreateDto userCreateDto = new UserCreateDto();
//        userCreateDto.setEmail("bob@gmail.com");
//        userCreateDto.setLogin("bob@gmail.com");
//        userCreateDto.setPassword("bob@gmail.com");
//        userCreateDto.setImageUrl("bob@gmail.com");
//
//        //when
//        MvcResult mvcResult = mockMvc.perform(post("/users")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(objectMapper.writeValueAsString(userCreateDto)))
//                .andExpect(status().isOk())
//                .andReturn();
//
//        byte[] bytes = mvcResult.getResponse().getContentAsByteArray();
//        UserFullDto savedUser = objectMapper.readValue(bytes, UserFullDto.class);
//
//        //then
//        Assertions.assertNotNull(savedUser);
//        //....
//    }
//
//    @Test
//    void testCreate_whenEmailIsEmpty() throws Exception {
//        //given
//        UserCreateDto userCreateDto = new UserCreateDto();
//        userCreateDto.setEmail(null);
//        userCreateDto.setLogin("bob@gmail.com");
//        userCreateDto.setPassword("bob@gmail.com");
//        userCreateDto.setImageUrl("bob@gmail.com");
//
//        //when
//        mockMvc.perform(post("/users")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(objectMapper.writeValueAsString(userCreateDto)))
//                .andExpect(status().isBadRequest());
//
//        //then
//    }
//
//    @Test
//    void testFindAll_happyPath() throws Exception {
//        //given
//        UserEntity userToSave1 = new UserEntity();
//        userToSave1.setEmail("bob@gmail.com");
//        userToSave1.setLogin("bo.com");
//        userToSave1.setPassword("bob@gmail.com");
//        userToSave1.setImageUrl("bob@gmail.com");
//
//        UserEntity userToSave2 = new UserEntity();
//        userToSave2.setEmail("bob@gma.com");
//        userToSave2.setLogin("bob@gmail.com");
//        userToSave2.setPassword("bob@gmail.com");
//        userToSave2.setImageUrl("bob@gmail.com");
//
//        repository.save(userToSave1);
//        repository.save(userToSave2);
//
//        //when
//        MvcResult mvcResult = mockMvc.perform(get("/users"))
//                .andExpect(status().isOk())
//                .andReturn();
//
//        byte[] bytes = mvcResult.getResponse().getContentAsByteArray();
//        List<UserFullDto> foundUsers = objectMapper.readValue(bytes, new TypeReference<List<UserFullDto>>() {});
//
//        //then
//        Assertions.assertNotNull(foundUsers);
//        Assertions.assertEquals(2,foundUsers.size());
//    }

}
