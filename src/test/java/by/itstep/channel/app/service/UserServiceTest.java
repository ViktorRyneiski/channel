package by.itstep.channel.app.service;

import by.itstep.channel.app.dto.user.UserCreateDto;
import by.itstep.channel.app.dto.user.UserFullDto;
import by.itstep.channel.app.dto.user.UserUpdateDto;
import by.itstep.channel.app.entity.UserEntity;
import by.itstep.channel.app.exception.EntityIsNotFoundException;
import by.itstep.channel.app.repository.UserRepository;
import by.itstep.channel.app.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserServiceTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @BeforeEach
    private void setUp(){
        userRepository.deleteAll();
    }

    @Test
    void testFindById_happyPath(){
        //given
        UserEntity userEntity = new UserEntity();
        userEntity.setLogin("Bob");
        userEntity.setPassword("12345678");
        userEntity.setEmail("Bob@gmail.com");
        userEntity.setImageUrl("http://pic.jpg");

        UserEntity saved = userRepository.save(userEntity);

        //when
        UserFullDto foundDto = userService.findById(saved.getId());

        //then
        Assertions.assertNotNull(foundDto);
        Assertions.assertEquals(saved.getId(),foundDto.getId());
        Assertions.assertEquals(saved.getEmail(),foundDto.getEmail());
    }

    @Test
    void testUpdate_happyPath(){
        //given
        UserCreateDto createDto = new UserCreateDto();
        createDto.setEmail("dod@dodson.com");
        createDto.setLogin("dod");
        createDto.setPassword("12345678");
        createDto.setImageUrl("http://dod@dodson.jpg");

        UserFullDto createdUser = userService.create(createDto);

        UserUpdateDto updateDto = new UserUpdateDto();
        updateDto.setEmail("updated");
        updateDto.setImageUrl("http://updated");
        updateDto.setId(createdUser.getId());

        //when
        UserFullDto updatedUser = userService.update(updateDto);


        //then
        Assertions.assertNotNull(updatedUser);
        Assertions.assertEquals(updatedUser.getId(),createdUser.getId());

        Assertions.assertNotEquals(updatedUser.getEmail(),createdUser.getEmail());
        Assertions.assertNotEquals(updatedUser.getProfileImageUrl(),createdUser.getProfileImageUrl());

        Assertions.assertEquals(updatedUser.getLogin(),createdUser.getLogin());
        Assertions.assertEquals(updatedUser.getEmail(),updateDto.getEmail());
        Assertions.assertEquals(updatedUser.getProfileImageUrl(),updateDto.getImageUrl());
    }

    @Test
    void testUpdate_whenNotFound(){
        //given
        UserUpdateDto updateDto = new UserUpdateDto();
        updateDto.setEmail("random@gmail.com");
        updateDto.setImageUrl("http://random.jpg");

        int notExistingId = 13;
        updateDto.setId(notExistingId);

        //when
        Exception exception = Assertions.assertThrows(EntityIsNotFoundException.class,
                () -> userService.update(updateDto));

        //then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    //2 юзера
}
