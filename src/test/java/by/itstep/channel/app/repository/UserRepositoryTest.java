package by.itstep.channel.app.repository;

import by.itstep.channel.app.entity.PostEntity;
import by.itstep.channel.app.entity.UserEntity;
import by.itstep.channel.app.repository.UserRepository;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
class UserRepositoryTest {

	@Autowired
	private UserRepository repository;

	private final Faker faker = new Faker();

	@Test
	void contextLoads() {

	}

	@Test
	void testCreate_happyPath() {
		//given
		UserEntity toSave1 = generateUser();

		//when
		UserEntity saved = repository.save(toSave1);



		//then
		Assertions.assertNotNull(saved);
		Assertions.assertNotNull(saved.getId());
	}

	@Test
	void testFindByLogin_happyPath(){
		//given
		UserEntity toSave = generateUser();
		UserEntity saved = repository.save(toSave);
		String loginToSearch = saved.getLogin();

		//when
		UserEntity found = repository.findByLogin(loginToSearch).get();

		//then
		Assertions.assertNotNull(found);
		Assertions.assertEquals(saved.getId(), found.getId());
		Assertions.assertEquals(saved.getLogin(), found.getLogin());

	}

	@Test
	void testFindById_happyPath(){
		//given
		UserEntity toSave = generateUser();
		UserEntity saved = repository.save(toSave);
		Integer idToSearch = saved.getId();

		//when
		UserEntity found = repository.findOneById(idToSearch);

		//then
		Assertions.assertNotNull(found);
		Assertions.assertEquals(saved.getId(), found.getId()); //мы ждем что они совпадут
		Assertions.assertEquals(saved.getLogin(), found.getLogin());//мы ждем что они совпадут

	}


	private UserEntity generateUser(){
		UserEntity userEntity = new UserEntity();
		userEntity.setLogin(faker.artist().name() + Math.random() * 100 );
		userEntity.setPassword(faker.random().toString());
		userEntity.setImageUrl(faker.avatar().image());
		System.out.println(userEntity.toString());

		return userEntity;
	}



}
